#include <stdio.h>
#include <gmp.h>
#include <unistd.h>
#include <math.h>
#include <stdlib.h>
#include "rsa_keygen.h"
#include "rsa_decode.h"
#include <curses.h>
#include <menu.h>
#include "main_menu.h"
#include <locale.h>
#include <form.h>
#include <limits.h>
#include <errno.h>
#include <math.h>
#define ARRAY_SIZE(a) (sizeof(a)/sizeof(a[0]))

char* nazwy_menu[] = {
  "Generuj nowy klucz RSA",
  "Wczytaj klucz z pliku",
  "Szyfruj plik",
  "Deszyfruj plik",
  "Wyjście"
};

int key_loaded = 0;
int public_key_loaded =0;
int private_key_loaded = 0;
mpz_t n, d, e;
char* SPLASH[] = {
  " ____  _                 _      ____  ____    _",
  "/ ___|(_)_ __ ___  _ __ | | ___|  _ \\/ ___|  / \\",
  "\\___ \\| | '_ ` _ \\| '_ \\| |/ _ \\ |_) \\___ \\ / _ \\",
  " ___) | | | | | | | |_) | |  __/  _ < ___) / ___ \\",
  "|____/|_|_| |_| |_| .__/|_|\\___|_| \\_\\____/_/   \\_\\",
  "                  |_|"
};

void init_ncurses() {
  setlocale(LC_CTYPE, ""); /* bez tego nie działa UTF8 w ncurses */
  initscr();
//start_color();
  cbreak();
  noecho();
  keypad(stdscr, TRUE);
}

void show_main_screen() {
  clear();
  int j;
  for(j=0; j < 6; ++j)
    mvprintw(1+j, 1, SPLASH[j]);
  mvprintw(7, 1, "wersja 0.1");
  mvprintw(8, 1, "(c) 2014 by Karol Piotrowski & Szczepan Lis");
  if(public_key_loaded)
    mvprintw(17, 1, "Załadowany klucz publiczny");
  if(private_key_loaded)
    mvprintw(18, 1, "Załadowany klucz prywatny");
}

void show_current_key(WINDOW* okno) {
  if(!key_loaded)
  {
    mvwprintw(okno, 1, 1, "Obecnie nie wczytano żadnego klucza.");
  };
}


void handle_key_load() {
  char file_name[255];
  int key_type = 0;
  clear();
  printw("Podaj nazwę pliku z kluczem: ");
  echo();
  getstr(file_name);
  noecho();
  int error_code = rsa_plaintext_to_key(file_name, &n, &d, &e, &key_type);
  switch(error_code) {
    case 0:
      key_loaded = 1;
      printw("Prawidłowo wczytano klucz. Dane klucza: \n");
      if(key_type==0) {
        private_key_loaded = 1;
        public_key_loaded = 0;
        printw("Typ: PRYWATNY\n");
      } else {
        public_key_loaded = 1;
        private_key_loaded = 0;
        printw("Typ: publiczny\n");
      };
      printw("Długość bloku n: %d bitów\n", mpz_sizeinbase(n, 2));
      printw("Długość bloku d/e: %d bitów\n", key_type ? mpz_sizeinbase(e, 2) : mpz_sizeinbase(d, 2));
      return 0;
      break;
    case 1:
      printw("To nie jest prawidłowy plik z kluczem RSA.\n");
      break;
    case 2:
//      printw("Plik nie istnieje bądź nie ma uprawnień do jego odczytu. \n");
      message_box(15, 15, "Plik nie istnieje bądź nie ma uprawnień do jego odczytu."); 
      break;
  };
}

void handle_key_generate() {
  clear();
//  if(key_loaded) {
//    printw(
  int error_code, key_size, good = 0;
  char keysize_input[20];
  char filename_input[256];
  do {
    printw("Wprowadź oczekiwany rozmiar klucza: ");
    echo();
    getnstr(keysize_input, 20);
    noecho();
    errno = 0;
    key_size = strtoul(keysize_input, NULL, 10);
    if(!errno && key_size > 0) {
      good = 1;
    } else
      printw("To nie jest prawidłowy rozmiar klucza.\n");
  } while(!good);
    printw("Rozpoczynam generowanie klucza RSA...\n");
    rsa_keygen(key_size, &n, &d, &e);
    printw("Zakończono generowanie klucza. \n");
    char* key_str = NULL;
    key_str = mpz_get_str(NULL, 16, n);
    printw("n = %s\n\n", key_str);
    free(key_str);
    key_str = mpz_get_str(NULL, 16, d);
    printw("d = %s\n\n", key_str);
    free(key_str);
    key_str = mpz_get_str(NULL, 16, e);
    printw("e = %s\n\n", key_str);
    free(key_str);
    public_key_loaded = 1;
    private_key_loaded = 1;
    printw("Czy chcesz zapisać wygenerowaną parę kluczy do plików? [T/n]");
    int response;
    int saving_to_file = 0;
    good = 0;
    while(!good) {
      response = getch();
      switch(response) {
        case '\n':
        case 't':
        case 'T': 
          good = 1;
          saving_to_file = 1;
          break;
        case 'n':
        case 'N':
          good = 1;
          break;
        default:
          break;
      };
    };
    if(saving_to_file) {
      good = 0;
      while(!good) {
        printw("Podaj nazwę pliku do zapisu klucza prywatnego: ");
        echo();
        getnstr(filename_input, 255);
        noecho();
        error_code = rsa_key_to_plaintext(filename_input, &n, &d, 0);
        switch(error_code) {
          case 0:
            good = 1;
            printw("Pomyślnie zapisano klucz prywatny w pliku %s.\n", filename_input);
            break;
          case 2:
            printw("Plik już istnieje, proszę wybrać inną nazwę pliku.\n");
            break;
        };
      };
      good = 0;
      while(!good) {
        printw("Podaj nazwę pliku do zapisu klucza publicznego: ");
        echo();
        getnstr(filename_input, 255);
        noecho();
        error_code = rsa_key_to_plaintext(filename_input, &n, &e, 1);
        switch(error_code) {
          case 0:
            good = 1;
            printw("Pomyślnie zapisano klucz publiczny w pliku %s. \n", filename_input);
            break;
          case 2:
            printw("Plik już istnieje, proszę wybrać inną nazwę pliku. \n");
            break;
        };
      };
    };        
     
}

int handle_cipher(int deciphering) {
  FILE* wejscie, *wyjscie;
  int good = 0;
  int right_key_loaded = deciphering ? private_key_loaded : public_key_loaded;
  clear();
    if(!right_key_loaded) {
      printw("Brak wczytanego klucza %s. \n", deciphering ? "prywatnego" : "publicznego");
      getch();
      return 0;
  };
  char file_name[256];
  while(!good) {
    printw("Podaj nazwę pliku do %s: ", deciphering ? "odszyfrowania" : "zaszyfrowania");
    getnstr(file_name, 255);
    if((wejscie = fopen(file_name, "r")) != NULL) {
      printf("Wczytano plik %s\n", file_name);
      good = 1;
    } else
      printw("Nieprawidłowa nazwa pliku lub plik nie istnieje. Spróbuj ponownie. \n");
  };
  good = 0;
  while(!good) {
    printw("Podaj plik docelowy: ");
    getnstr(file_name, 255);
    if(fopen(file_name,"r") == NULL) {
      printw("Ustalono %s jako plik do zapisu.\n", file_name);
      good =1 ;
      wyjscie = fopen(file_name, "w");
    } else {
      fclose(wyjscie);
      printw("Plik %s już istnieje. Proszę podać inną nazwę pliku.\n", file_name);
    };
   };
  int key_size = mpz_sizeinbase(n, 2);
  int input_chunk = floor((key_size-1)/8);
  int output_chunk = 1 + floor(key_size/8);
  if(!deciphering) {
    rsa_encode_file(wejscie, wyjscie, &n, &e, input_chunk, output_chunk);
  } else {
    rsa_decode_file(wejscie, wyjscie, &n, &d, output_chunk, input_chunk);
  };
  fclose(wejscie);
  fclose(wyjscie);
}
 
    
int main(int argc, char* argv[]) {
  mpz_init(n);
  mpz_init(d);
  mpz_init(e);
  int sel_menu_pos = 0;
  int quitting = 0;
  init_ncurses();
  show_main_screen();
  while(!quitting) {
    sel_menu_pos = single_choice_menu(10, 1, nazwy_menu, ARRAY_SIZE(nazwy_menu));
    switch(sel_menu_pos) {
      case 0:
        handle_key_generate();
        break;
      case 1:
        handle_key_load();
        break;
      case 2:
        handle_cipher(0);
        break;
      case 3:
        handle_cipher(1);
        break;
      case 4:
        quitting = 1;
        break;
      };
   show_main_screen(); 
  };
  mpz_clear(n);
  mpz_clear(d);
  mpz_clear(e);
  endwin();
  return 0;
}
