#include <curses.h>
#include <menu.h>
#include <stdlib.h>
#include <string.h>

/*! wyświetlanie menu pojedynczego wyboru
 *
 * Funkcja zwraca numer wybranej pozycji w menu (poczynając od zera).
 * \param left_y,left_x współrzędne lewego górnego rogu.
 * \param positions[] tablica pozycji
 * \param ilość opcji do wyboru
 */

int message_box(int left_y, int left_x, char komunikat[])
 { 
  WINDOW* msgbox = newwin(5, strlen(komunikat)+4, left_y, left_x);
  box(msgbox, 0, 0);
  mvwprintw(msgbox, 2, 2, "%s", komunikat);
  mvwprintw(msgbox, strlen(komunikat+3)/2, 3, "OK");
  wrefresh(msgbox);
  getch();
  delwin(msgbox);
  refresh();
 };

  

int single_choice_menu(int left_y, int left_x, char* positions[], int no_choices) {
  ITEM** menu_items;
  MENU* menu_content;
  int c, i, j, selected_pos;
  menu_items = (ITEM**) calloc(no_choices+1, sizeof(ITEM*));
  for(j=0; j<no_choices; ++j)
    menu_items[j] = new_item(positions[j], "");
  menu_items[no_choices] = (ITEM*) NULL;
  menu_content = new_menu(menu_items);
  int needed_y, needed_x;
  scale_menu(menu_content, &needed_y, &needed_x);
  WINDOW* win = newwin(needed_y, needed_x, left_y, left_x);
  keypad(win, TRUE);
  set_menu_win(menu_content, win);
  set_menu_sub(menu_content, win);
  post_menu(menu_content);
  refresh();
  wrefresh(win);
  while((c = wgetch(win)) != '\n') {
    switch(c) {
      case KEY_DOWN:
        menu_driver(menu_content, REQ_DOWN_ITEM);
        break;
      case KEY_UP:
        menu_driver(menu_content, REQ_UP_ITEM);
        break;
    };
    wrefresh(win);
  };
  selected_pos = item_index(current_item(menu_content));
  unpost_menu(menu_content);
  free_menu(menu_content);
  for(i=0; i < no_choices; ++i)
    free_item(menu_items[i]);
  delwin(win);
  return selected_pos;
}
