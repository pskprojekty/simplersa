#include <gmp.h>
#include <stdio.h>
#define ASCII 256
/*! kodowanie/dekodowanie RSA
 *
 * Funkcja wykonuje kodowanie/dekodowanie zgodnie
 * z algorytmem RSA. Ze względu na to, że dekodowanie/
 * kodowanie przebiega tak samo, różni się jedynie użyciem
 * klucza publicznego/prywatnego przy podnoszeniu do potęgi,
 * zawarte zostało w jednej procedurze.
 * \param message komunikat do zakodowania/zdekodowania
 * \param result wskaźnik na wynik operacji
 * \param n część n klucza (n,d)/(n,e)
 * \param d część d/e klucza prywatnego/publicznego
 */

void rsa_dec(mpz_t* message, mpz_t* result, mpz_t n, mpz_t d) {
	mpz_powm_sec(*result, *message, d, n);
}

/*! kodowanie pliku binarnego zgodnie z algorytmem RSA.
 *
 * Funkcja ta pobiera ze strumienia input_file bloki znaków o wielkości
 * określonej przez block_size, koduje je jako liczbę całkowitą w której
 * kolejne znaki są cyframi w jej rozwinięciu przy podstawie 256.
 * \param input_file strumień wejściowy
 * \param output_file strumień wyjściowy
 * \param n część klucza (n,e)
 * \param e publiczna część klucza
 * \param block_size wielkość bloku wejściowego w bajtach
 * \param out_block_size wielkość bloku wyjściowego w bajtach
 * \todo zamienić ręczne rzeźbienie z konwersją blokową na mpz_import
 */
void rsa_encode_file(FILE* input_file, FILE* output_file, mpz_t* n, mpz_t* e, int block_size, int out_block_size) {
  mpz_t message, result, rem, power, pot, temp;
  mpz_init(message);
  mpz_init(result);
  mpz_init(power);
  mpz_init_set_ui(pot,1);
  mpz_init(temp);
  int readed_blocks,j;
  int padded_bytes = 0;
  unsigned char block[block_size];
  unsigned char out_block[out_block_size];
  while(block_size == (readed_blocks = fread(&block[0], 1, block_size, input_file))) {
    mpz_set_ui(message, 0);
    if(readed_blocks < block_size) {
      padded_bytes = block_size - readed_blocks;
      for(j=readed_blocks; j < block_size; ++j)
        block[j] = (unsigned char) padded_bytes;
    };
    mpz_set_ui(pot,1);
    for(j=0; j<block_size; ++j) {
      mpz_mul_ui(temp, pot, block[j]);
      mpz_add(message, message, temp);
      mpz_mul_ui(pot, pot, ASCII);
    };
    mpz_powm(result, message, *e, *n);
    for(j=0; j<out_block_size; ++j) {
      out_block[j] = (unsigned char) mpz_tdiv_q_ui(result, result, ASCII);
    };
    fwrite(out_block, 1, out_block_size, output_file);
  };
  if(padded_bytes==0) {
    for(j=0; j < block_size; ++j)
      block[j] = (unsigned char) block_size;
    fwrite(out_block, 1, out_block_size, output_file);
  };
  mpz_clear(message);
  mpz_clear(result);
  mpz_clear(pot);
  mpz_clear(temp);
  mpz_clear(power);
}

void rsa_decode_file(FILE* input_file, FILE* output_file, mpz_t* n, mpz_t* d, int block_size, int out_block_size) {
  mpz_t message, result, remainder, power, pot, temp;
  mpz_init(message);
  mpz_init(result);
  mpz_init(remainder);
  mpz_init(power);
  mpz_init(pot);
  mpz_init(temp);
  unsigned char block[block_size];
  unsigned char out_block[out_block_size];
  int j, readed_bytes;
  int padded_bytes = 0;
  while((readed_bytes = fread(&block[0], 1, block_size, input_file))) {
    mpz_set_ui(message, 0);
    /* w przypadku, gdy jesteśmy na końcu, pobierz miejsce obcięcia ciągu */
    if(readed_bytes < block_size) {
      padded_bytes = block[readed_bytes-1];
    };
    /* zapisz blok jako dużą liczbę */
    mpz_set_ui(pot, 1);
    for(j=0; j<block_size; ++j) {
      mpz_mul_ui(temp, pot, block[j]);
      mpz_add(message, message, temp);
      mpz_mul_ui(pot, pot, ASCII);
    };
    /* właściwe RSA */
    mpz_powm(result, message, *d, *n);
    /* zmień zakodowaną liczbę z powrotem w ciąg bajtów */
    for(j=0; j<out_block_size; ++j) {
      out_block[j] = (unsigned char) mpz_tdiv_q_ui(result, result, ASCII);
    };
    /* tutaj bierzemy pod uwagę padding */
    fwrite(&out_block[0], 1, out_block_size - padded_bytes, output_file);
  };
  mpz_clear(message);
  mpz_clear(result);
  mpz_clear(pot);
  mpz_clear(temp);
  mpz_clear(power);
}
